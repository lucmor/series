const gui = require('./gui');
const utils = require('./utils');

const config = {
    desarrollo: (process.env.DESARROLLO == undefined) ? true : false
};

utils.verificarActualizador();

gui.montarGrafica();

let win = gui.crearVentana(config)