const ParserOpenLoadCo = require('./ParserServidores/ParserOpenLoadCo');
const ParserOneFichier = require('./ParserServidores/ParserOneFichier');
//const ParserFileFactory = require('./ParserServidores/ParserFileFactory');
//const ParserUlTo = require('./ParserServidores/ParserUlTo');
const ParserRapidGator = require('./ParserServidores/ParserRapidGator');
const ParserRockFile = require('./ParserServidores/ParserRockFile');

class ParserLinks {
    constructor() {
        this.parser = null;
    }

    verificarEnlace(enlace) {
        if (this.verificarServidor(enlace)) {
            return this.parser.linkFuncionando(enlace);
        } else {
            return new Promise((resolve) => {
                resolve(false);
            });
        }
    }

    verificarServidor(enlace) {
        if (enlace.includes("openload.co")) {
            this.parser = new ParserOpenLoadCo();

            return true;
        } else if (enlace.includes("1fichier.com")) {
            this.parser = new ParserOneFichier();

            return true;
        } else if (enlace.includes("rapidgator.net")) {
            this.parser = new ParserRapidGator();

            return true;
        } else if (enlace.includes("rockfile.co")) {
            this.parser = new ParserRockFile();

            return true;
        /*} else if(enlace.includes("ul.to")) {
            this.parser = new ParserUlTo();

            return true;
        }*/
        /*} else if (enlace.includes("filefactory.com")) {
            this.parser = new ParserFileFactory();

            return true;*/
        } else {
            return false;
        }
    }
}

module.exports = ParserLinks;