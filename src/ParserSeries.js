const rp = require('request-promise');
const $ = require('cheerio');

const ParserLinks = require('./ParserLinks');

class ParserSeries {
    constructor() {
        this.parserLinks = new ParserLinks();
    }

    obtenerBusquedas(serieBuscar) {
        this.requestPromise = rp("https://seriesblanco.org/?s=" + serieBuscar);
        
        return this.requestPromise.then((html) => {
            let ulListaBuscado = $('nav.display-default>ul', html); // Nav de las busquedas

            return $('li>div>a', ulListaBuscado).map((i, elem) => { // link de cada item
                return {
                    nombre: $('img', elem).attr("alt"),
                    url: $(elem).attr("href")
                };
            }).get();
        });
    }

    obtenerTemporadas(url) {
        this.requestPromise = rp(url);

        return this.requestPromise.then((html) => {
            return $("#accordion>div>div.panel-heading", html).get();
        });
    }

    obtenerCapitulos(temporada) {
        return this.requestPromise.then((html) => {
            return $("#collapse" + (temporada + 1) + ">div>table>tbody>tr>td>a", html).map((i, elem) => {
                return {
                    nombre: 'Capitulo ' + (i + 1),
                    url: $(elem).attr("href")
                };
            }).get();
        });
    }

    obtenerLink(url) {
        this.requestPromise = rp(url);

        return new Promise((resolve, reject) => {
            this.requestPromise.then((html) => {
                $("#collapseV>div>table>tbody>tr>td>div>span>a", html).each((i, elem) => {
                    let enlace = $(elem).attr("data-enlace");
                    
                    this.parserLinks.verificarEnlace(enlace).then((funciona) => {
                        if (funciona) {
                            resolve(enlace);
                        }
                    });
                });
            });
        });
    }
}

module.exports = ParserSeries;