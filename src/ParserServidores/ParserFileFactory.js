const rp = require('request-promise');
const $ = require('cheerio');

class ParserOneFichier {
    linkFuncionando(enlace) {
        let requestPromise = rp(enlace);

        return requestPromise.then((html) => {
            let funciona = true;

            $("embed", html).each((i, elem) => {
                if ($(elem).attr("src").includes("error")) {
                    funciona = false;
                }
            });

            return funciona;
        }).catch((err) => {
                
        });
    }
}

module.exports = ParserOneFichier;