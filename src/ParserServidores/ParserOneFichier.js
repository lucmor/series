const rp = require('request-promise');
const $ = require('cheerio');

class ParserOneFichier {
    linkFuncionando(enlace) {
        let requestPromise = rp(enlace);

        return requestPromise.then((html) => {
            let funciona = true;

            $(".bloc2", html).each((i, elem) => {
                if ($(elem).text().includes("deleted")) {
                    funciona = false;
                }
            });

            return funciona;
        }).catch((err) => {
                
        });
    }
}

module.exports = ParserOneFichier;