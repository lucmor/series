const rp = require('request-promise');
const $ = require('cheerio');

class ParserOneFichier {
    linkFuncionando(enlace) {
        let requestPromise = rp(enlace);

        return requestPromise.then((html) => {
            let funciona = true;

            $("h3", html).each((i, elem) => {
                if ($(elem).text().includes("404")) {
                    funciona = false;
                }
            });

            return funciona;
        }).catch((err) => {
                
        });
    }
}

module.exports = ParserOneFichier;