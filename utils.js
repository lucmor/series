const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const axios = require('axios');
const md5File = require('md5-file');

const urlActualizacion = "http://nahuelmorata.ml/actualizaciones/";

function obtenerOS() {
    let os = "";

    if (process.platform === "win32") {
        os = "windows";
    } else if (process.platform === "linux") {
        os = "linux";
    }

    return os;
}

function obtenerArch() {
    let arch = "";

    if (process.arch == "x32") {
        arch = "x86";
    } else if (process.arch == "x64") {
        arch = "x64";
    }

    return arch;
}

function descargarActualizador(urlActualizacion) {
    let version = "0.0.0";
    axios.get(urlActualizacion + "actualizador-" + obtenerOS() + "-" + obtenerArch() + "/" + version).then((response) => {
        if (response.data.hayActualizacion) {
            let extension = "";

            if (obtenerOS() == "windows") {
                extension = ".exe";
            }

            const pathActualizador = path.resolve(__dirname, 'Actualizador' + extension);

            const writer = fs.createWriteStream(pathActualizador);

            axios({
                url: response.data.url,
                method: "get",
                responseType: "stream"
            }).then((response) => {
                response.data.pipe(writer);
            });
        }
    });
}

function verificarActualizador() {
    let os = obtenerOS();
    let arch = obtenerArch();
    let actualizador = "Actualizador";

    if (os == "windows") {
        actualizador += ".exe"
    }

    let existeActualizador = fs.existsSync(actualizador);

    if (existeActualizador) {
        let hash = md5File.sync(actualizador);

        axios.get(urlActualizacion + "verificar/actualizador-" + os + "-" + arch + "/" + hash).then((response) => {
            if (!response.data.correcto) {
                descargarActualizador(urlActualizacion);
            }
        });
    } else {
        descargarActualizador(urlActualizacion);
    }
}

module.exports = {
    verificarActualizador: verificarActualizador
};