const { app, BrowserWindow } = require('electron');
const finalhandler = require('finalhandler');
const http = require('http');
const serveStatic = require('serve-static');

const puerto = 24503;

function createWindow (config) {
    win = new BrowserWindow({ width: 800, height: 600 })

    win.loadURL('http://localhost:' + puerto + '/');
    
    if (config.desarrollo) {
        win.webContents.openDevTools();
    }

    win.on('closed', () => {
        win = null
    });
    
    return win;
}

function montarGrafica() {
    const serve = serveStatic(__dirname + '/gui/build');

    const server = http.createServer((req, res) => {
      serve(req, res, finalhandler(req, res));
    });

    server.listen(puerto);
}

function crearVentana(config) {
    app.on('ready', () => { createWindow(config); });

    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

    app.on('activate', () => {
        if (win === null) {
            createWindow()
        }
    });
}

module.exports = {
    crearVentana: crearVentana,
    montarGrafica: montarGrafica
};